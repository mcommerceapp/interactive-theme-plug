package com.application.interactivethemeplug.model



data class ApplicationThemeModel(
    val data: Data,
    val message: String,
    val statusCode: String
)

data class Data(
    val id: Int,
    val primaryColor: String,
    val secondryColor: String,
    val statusBarColor: String,
    val textColor: String
)