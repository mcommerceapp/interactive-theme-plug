package com.application.interactivethemeplug.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
/*import android.support.design.button.MaterialButton
import android.support.design.widget.TextInputLayout*/
import android.support.v4.content.ContextCompat
import android.support.v7.widget.Toolbar
import android.view.WindowManager
import android.widget.Button
import android.graphics.drawable.GradientDrawable
import android.support.design.widget.TextInputLayout
import com.application.interactivethemeplug.R

import android.content.res.ColorStateList

import android.support.v4.graphics.drawable.DrawableCompat
import android.widget.EditText
import java.lang.Exception


fun Activity.setStatusBarColor() {
    try {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            val statusBarColor = Color.parseColor(ApplicationThemeUtils.PRIMARY_DARK)
            if (statusBarColor == Color.BLACK && window.getNavigationBarColor() === Color.BLACK) {
                window.clearFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            } else {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            }
            window.statusBarColor = statusBarColor
        }
    }
    catch (e:Exception){
        e.printStackTrace()
    }

}

fun setButtonTheme(button: Button) {

    button.getBackground().setColorFilter(Color.parseColor(ApplicationThemeUtils.PRIMARY_COLOR), PorterDuff.Mode.SRC_ATOP);
    button.setTextColor(Color.parseColor(ApplicationThemeUtils.TEXT_COLOR))
}

fun setTextInputTheme(textInputLayout: TextInputLayout,editText:EditText) {

    var color = Color.parseColor(ApplicationThemeUtils.PRIMARY_COLOR)


    //editText.getBackground().setColorFilter(Color.parseColor(ApplicationThemeUtils.PRIMARY_COLOR), PorterDuff.Mode.SRC_ATOP);
   //textInputLayout.boxStrokeColor = Color.parseColor(ApplicationThemeUtils.PRIMARY_COLOR)


/*    val background = textInputLayout.getEditText()!!.getBackground()
    DrawableCompat.setTint(background, Color.parseColor(ApplicationThemeUtils.PRIMARY_COLOR))
    textInputLayout.getEditText()?.setBackground(background)


    textInputLayout.getEditText()!!.setHighlightColor(Color.parseColor(ApplicationThemeUtils.PRIMARY_COLOR))
    textInputLayout.getEditText()!!.setHintTextColor(Color.parseColor(ApplicationThemeUtils.PRIMARY_COLOR))*/



}



fun setToolbarColor(toolbar: Toolbar) {
    toolbar.setBackgroundColor(Color.parseColor(ApplicationThemeUtils.PRIMARY_COLOR))
    toolbar.setTitleTextColor(Color.parseColor(ApplicationThemeUtils.TEXT_COLOR))
}
