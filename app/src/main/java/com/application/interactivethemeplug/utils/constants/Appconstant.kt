package com.application.interactivethemeplug.utils.constants

object SHARED_PREFERENCES{
    val FILE_NAME = "Shared_preference"
    val APPLICATION_THEME_OBJECT = "app_theme"
}