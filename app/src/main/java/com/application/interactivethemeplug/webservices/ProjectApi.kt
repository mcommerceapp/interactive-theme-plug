package com.application.interactivethemeplug.webservices


import com.application.interactivethemeplug.model.ApplicationThemeModel
import com.application.interactivethemeplug.utils.ApplicationThemeUtils
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ProjectApi {

    /*   @GET("Upswing/GetQuestionAnswerList")
       fun getQuestionAnswerListApi(
               @Query("CategoryId") CategoryId: Int,
               @Query("skip") skip: Int,
               @Query("take") take: Int
       ): Call<OfflineQuestionAnswerApiData>
   */

    /* @POST("Subscription/Braintree")
    fun postSubscriptionDetailsToServer(
            //@Body paypalPaymentModel: PaypalPaymentModel
    ): retrofit2.Call<JsonObject>

    @GET("Subscription/GetPackages")
    fun getPackageListApi(
            @Query("userId") userId: Int
    ): retrofit2.Call<JsonObject>*/


 /*   @GET("Configuration/GetAppTheme")
    fun getApplicationThemeApi(
    ): retrofit2.Call<JsonObject>*/

    @get:GET("configuration/getapptheme")
    val myJSON: Call<ApplicationThemeModel>

}