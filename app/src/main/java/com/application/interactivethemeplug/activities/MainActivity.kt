package com.application.interactivethemeplug.activities

import android.content.Intent
import android.graphics.Color
import android.opengl.Visibility
import android.os.Build

import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import com.application.interactivethemeplug.R
import com.application.interactivethemeplug.model.ApplicationThemeModel
import com.application.interactivethemeplug.model.Data
import com.application.interactivethemeplug.sharedPreferences.SharedPreferences
import com.application.interactivethemeplug.utils.*
import com.application.interactivethemeplug.utils.validation.isNetworkAccessible
import com.application.interactivethemeplug.webservices.ProjectApi
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_progress_bar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        if (isNetworkAccessible()) {
            showProgressBar()
            callApplicationThemeApi()
        } else {

            hideProgressBar()
            layout_main.visibility = View.GONE
            showAlertDialog()
        }


        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                textUsernameInputLayout.isErrorEnabled = false
            }

        })

        editTextPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                textPasswordInputLayout.isErrorEnabled = false
            }
        })





        button.setOnClickListener {
            if (TextUtils.isEmpty(editText.text)) {
                textUsernameInputLayout.isErrorEnabled = true
                textUsernameInputLayout.error = getString(R.string.txt_field_cannot_be_blank)
            }
            if (TextUtils.isEmpty(editTextPassword.text)) {
                textPasswordInputLayout.isErrorEnabled = true
                textPasswordInputLayout.error = getString(R.string.txt_field_cannot_be_blank)
            } else {
                startActivity(Intent(this, DashboardActivity::class.java))

            }
        }
    }

    private fun showAlertDialog() {
        val builder = AlertDialog.Builder(this@MainActivity)

        builder.setMessage("Please check internet connection")

        builder.setPositiveButton("ok") { dialog, which ->
            if (isNetworkAccessible()) {
                showProgressBar()
                callApplicationThemeApi()
            } else {
                hideProgressBar()
                layout_main.visibility = View.GONE
                if (isNetworkAccessible()) {
                    callApplicationThemeApi()
                } else {
                    try {
                        getDataFromSharedPreference()
                    } catch (e: Exception) {
                        layout_main.visibility = View.VISIBLE
                        toolbar.title = getString(R.string.app_name)
                    }
                }
            }

        }
        val dialog: AlertDialog = builder.create()

        dialog.show()
        dialog.setCancelable(false)

    }

    private fun getDataFromSharedPreference() {
        layout_main.visibility = View.VISIBLE

        if (SharedPreferences.getInstance(this).tokenDataPreference.equals("")) {
            hideProgressBar()
            layout_main.visibility = View.GONE
            showAlertDialog()
        } else {
            var sharedPreferences = SharedPreferences.getInstance(this).tokenDataPreference
            ApplicationThemeUtils.PRIMARY_COLOR = sharedPreferences.primaryColor
            ApplicationThemeUtils.PRIMARY_DARK = sharedPreferences.statusBarColor
            ApplicationThemeUtils.SECONDARY_COLOR = sharedPreferences.secondryColor
            ApplicationThemeUtils.TEXT_COLOR = sharedPreferences.textColor
            setActivityTheme()
        }
    }

    private fun setActivityTheme() {
        setStatusBarColor()
        toolbar.title = getString(R.string.app_name)
        setToolbarColor(toolbar = toolbar)
        setButtonTheme(button)
        setTextInputTheme(textUsernameInputLayout, editText)
        setTextInputTheme(textPasswordInputLayout, editTextPassword)
    }

    private fun callApplicationThemeApi() {
        showProgressBar()
        val clientInstance = ServiceGenerator.createService(ProjectApi::class.java)
        val call = clientInstance.myJSON
        call.enqueue(object : Callback<ApplicationThemeModel> {
            override fun onFailure(call: Call<ApplicationThemeModel>?, t: Throwable?) {
                Log.e("FailurResponse", t.toString())

                hideProgressBar()

            }

            @RequiresApi(Build.VERSION_CODES.O)
            override fun onResponse(call: Call<ApplicationThemeModel>?, response: Response<ApplicationThemeModel>?) {
                if (isNetworkAccessible()) {
                    hideProgressBar()
                    Log.e("SucessResponse", response.toString())
                    if (response?.body()?.statusCode.equals("10")) {
                        updatePreference(response?.body()!!.data)
                        ApplicationThemeUtils.PRIMARY_COLOR = response?.body()?.data?.primaryColor.toString()
                        ApplicationThemeUtils.PRIMARY_DARK = response?.body()?.data?.statusBarColor.toString()
                        ApplicationThemeUtils.SECONDARY_COLOR = response?.body()?.data?.secondryColor.toString()
                        ApplicationThemeUtils.TEXT_COLOR = response?.body()?.data?.textColor.toString()
                        setActivityTheme()
                    }

                } else {
                    hideProgressBar()

                    Toast.makeText(this@MainActivity, "Please check internet connection", Toast.LENGTH_SHORT).show()
                }
            }

        })


    }

    fun hideProgressBar() {
        progressBar.visibility = View.GONE
        layout_main.visibility = View.VISIBLE

    }

    fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
        layout_main.visibility = View.GONE


    }

    private fun updatePreference(themeDatavalue: Data) {
        var themeData = Data(0, "", "", "", "")
        themeData = themeDatavalue
        SharedPreferences.getInstance(this).tokenDataPreference = themeData

    }

}
