package com.application.interactivethemeplug.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.application.interactivethemeplug.R
import com.application.interactivethemeplug.utils.setStatusBarColor
import com.application.interactivethemeplug.utils.setToolbarColor
import kotlinx.android.synthetic.main.activity_dashboard
.*

class DashboardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        try {
            setStatusBarColor()
            toolbar.title = getString(R.string.app_name)
            setToolbarColor(toolbar = toolbar)
        }catch (e:Exception){
            toolbar.title = getString(R.string.app_name)
        }


    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}
