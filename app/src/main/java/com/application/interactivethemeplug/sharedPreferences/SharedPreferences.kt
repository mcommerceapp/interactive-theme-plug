package com.application.interactivethemeplug.sharedPreferences

import android.content.Context
import com.application.interactivethemeplug.model.ApplicationThemeModel
import com.application.interactivethemeplug.model.Data
import com.application.interactivethemeplug.utils.constants.SHARED_PREFERENCES
import com.application.interactivethemeplug.utils.constants.SHARED_PREFERENCES.APPLICATION_THEME_OBJECT
import com.application.interactivethemeplug.utils.constants.SHARED_PREFERENCES.FILE_NAME
import com.google.gson.Gson

import com.google.gson.GsonBuilder

/**
 * shared preference class for data persistence
 */
class SharedPreferences private constructor(context: Context) {

    private var appSharedPreferences: android.content.SharedPreferences? = null
    private lateinit var gson: Gson


    var tokenDataPreference: Data
        get() {
            val tokenDataString = appSharedPreferences!!.getString(APPLICATION_THEME_OBJECT, "")
            return gson.fromJson(tokenDataString, Data::class.java)
        }
        set(tokenData) {
            val tokenDataString = gson.toJson(tokenData)
            appSharedPreferences!!.edit().putString(APPLICATION_THEME_OBJECT, tokenDataString).apply()
        }

    init {
        if (appSharedPreferences == null) {
            appSharedPreferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE)
            gson = Gson()
        }
    }

    fun clear() {
        appSharedPreferences!!.edit().clear().apply()
    }

    companion object {

        fun getInstance(context: Context): SharedPreferences {
            return SharedPreferences(context)
        }
    }
}